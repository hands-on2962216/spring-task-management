package com.enigma.task_management_spring.service.impl;

import com.enigma.task_management_spring.entity.Task;
import com.enigma.task_management_spring.repo.TaskRepo;
import com.enigma.task_management_spring.repo.impl.TaskRepoImpl;
import com.enigma.task_management_spring.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TaskServiceImpl implements TaskService {
    private final TaskRepo repo;

    @Autowired
    public TaskServiceImpl(TaskRepoImpl repo) {
        this.repo = repo;
    }

    @Override
    public void create(Task object) {
        if (object == null || object.getDesc().isBlank() || object.getDesc().isEmpty())
            throw new RuntimeException("task is required");
        try {
            repo.save(object);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
