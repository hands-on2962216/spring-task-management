package com.enigma.task_management_spring.service;

import com.enigma.task_management_spring.entity.Task;

public interface TaskService {
    void create(Task object);
}
