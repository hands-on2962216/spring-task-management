package com.enigma.task_management_spring.entity;

public enum Color {
    RESET  ("\u001B[0m"),
    RED  ("\u001B[31m"),
    GREEN  ("\u001B[32m"),
    YELLOW  ("\u001B[33m");
    private String value;

    Color(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
