package com.enigma.task_management_spring.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "m_task")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "task_seq")
    @SequenceGenerator(name = "task_seq", sequenceName = "m_task_id_seq", allocationSize = 1)
    private Integer id;
    @Column(name = "description", nullable = false)
    private String desc;

    public Task(Integer id, String desc) {
        this.id = id;
        this.desc = desc;
    }

    public Task() {
    }

    public Task(String desc) {
        this.desc = desc;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
