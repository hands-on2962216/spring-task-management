package com.enigma.task_management_spring.exception;


import com.enigma.task_management_spring.entity.Color;

public class BookNotFoundException extends Exception{
    public BookNotFoundException(String message) {
        super(Color.RED.getValue() + "Buku dengan "+ message +" tidak ditemukan" + Color.RESET.getValue());
    }
}
