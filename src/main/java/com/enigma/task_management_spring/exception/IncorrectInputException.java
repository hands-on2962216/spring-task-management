package com.enigma.task_management_spring.exception;

import com.enigma.task_management_spring.entity.Color;

public class IncorrectInputException extends Exception {
    public static final String message = "Input tidak sesuai";
    public IncorrectInputException(String message) {
        super(Color.RED.getValue() + message + Color.RESET.getValue());
    }
}
