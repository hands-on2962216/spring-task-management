package com.enigma.task_management_spring;

import com.enigma.task_management_spring.util.JpaUtil;
import com.enigma.task_management_spring.view.TaskView;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {

        ApplicationContext context = new AnnotationConfigApplicationContext(JpaUtil.class);
        JpaUtil jpaUtil = (JpaUtil) context.getBean("jpaUtil");
        TaskView taskView = (TaskView) context.getBean("taskView");
        taskView.run();
        jpaUtil.shutdown();
    }
}