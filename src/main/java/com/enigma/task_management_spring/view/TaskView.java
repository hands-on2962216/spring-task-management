package com.enigma.task_management_spring.view;

import com.enigma.task_management_spring.entity.Task;
import com.enigma.task_management_spring.service.TaskService;
import com.enigma.task_management_spring.util.ScanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TaskView {
    private final TaskService service;

    @Autowired
    public TaskView(TaskService service) {
        this.service = service;
    }
    public void run() {
        String option = ScanUtil.inputString("Pilih Menu");
        switch (option) {
            case "1" -> saveView();
            case "x", "X" -> {
            }
            default -> System.out.println("Pilihan tidak sesuai!");
        }
    }

    private void saveView() {
        String task = ScanUtil.inputString("Inputkan Task");
        Task taskModel = new Task(null, task);
        try {
            service.create(taskModel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
