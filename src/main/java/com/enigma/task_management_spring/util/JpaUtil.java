package com.enigma.task_management_spring.util;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@Component
@ComponentScan("com.enigma.task_management_spring")
public class JpaUtil {
    private final String PERSISTENCE_UNIT_NAME = "java.tms";
    private EntityManagerFactory factory;

    public JpaUtil() {
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
    }

    @Bean
    public EntityManager getEntityManager() {
        if (factory.isOpen()) {
            return factory.createEntityManager();
        }
        return null;
    }

    public void shutdown() {
        if (factory != null && factory.isOpen()) {
            factory.close();
        }
        factory = null;
    }
}
