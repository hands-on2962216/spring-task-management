package com.enigma.task_management_spring.util;


import com.enigma.task_management_spring.entity.Color;
import com.enigma.task_management_spring.exception.IncorrectInputException;

import java.util.Scanner;

public class ScanUtil {

    public static String inputString(String info) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.printf("%s : ", info);
            String input = scanner.nextLine();
            if (input.isEmpty() || input.isBlank())
                continue;
            else {
                return input;
                // scanner.close();
            }
        }
    }
    public static String inputTitleWithValidation(Boolean isEdit, Object o) {
        Scanner scanner = new Scanner(System.in);
        String example = "contoh : [Harry Potter]\t\t";
        String info = isEdit ? "[" + o + "]\t" : example;
        while (true) {
            System.out.printf("""
                    Judul	(min 3 karakter, maks 50 karakter)  %s  :""" + " ", info);
            String input = scanner.nextLine();
            if ( isEdit && (input.isBlank() || input.isEmpty()) ) return (String) o;
            if (input.length() >= 3 && input.length() <= 50) return input;
            else {
                System.out.println(Color.RED.getValue() + IncorrectInputException.message + Color.RESET.getValue());
                continue;
            }
        }
    }

    public static String inputPublisherWithValidation(Boolean isEdit, Object o) {
        Scanner scanner = new Scanner(System.in);
        String example = "contoh : [Diva Press]\t\t";
        String info = isEdit ? "[" + o + "]\t" : example;
        while (true) {
            System.out.printf("""
                    Penerbit (min 3 karakter, maks 50 karakter) %s  :""" + " ", info);
            String input = scanner.nextLine();
            if ( isEdit && (input.isBlank() || input.isEmpty()) ) return (String) o;
            if (input.length() >= 3 && input.length() <= 50) return input;
            else {
                System.out.println(Color.RED.getValue() + IncorrectInputException.message + Color.RESET.getValue());
                continue;
            }
        }
    }

    public static String inputAuthorWithValidation(Boolean isEdit, Object o) {
        Scanner scanner = new Scanner(System.in);
        String example = "contoh : [J.K. Rowling]\t\t";
        String info = isEdit ? "[" + o + "]\t" : example;
        while (true) {
            System.out.printf("""
                    Penulis (min 3 karakter, maks 50 karakter) %s  :""" + " ", info);
            String input = scanner.nextLine();
            if ( isEdit && (input.isBlank() || input.isEmpty()) ) return (String) o;
            if (input.length() >= 3 && input.length() <= 50) return input;
            else {
                System.out.println(Color.RED.getValue() + IncorrectInputException.message + Color.RESET.getValue());
                continue;
            }
        }
    }

    public static int inputYearWithValidation(Boolean isEdit, Object o) {
        Scanner scanner = new Scanner(System.in);
        String example = "contoh : [2005]\t\t\t\t";
        String info = isEdit ? "[" + o + "]\t" : example;
        while (true) {
            System.out.printf("""
                    Tahun Terbit (harus positif)\t\t\t\t%s  :""" + " ", info);
            String input = (scanner.nextLine());
            try {
                if ( isEdit && (input.isBlank() || input.isEmpty()) ) return (Integer) o;
                if (Integer.parseInt(input) > 0 ) return Integer.parseInt(input);
            } catch (Exception e) {
                System.out.println(Color.RED.getValue() + IncorrectInputException.message + Color.RESET.getValue());
                continue;
            }
        }
    }


    public static Integer inputIntUtil(Object object, Boolean isEdit) {
        Scanner scanner = new Scanner(System.in);
        String info = (object != null) ? "[" + object + "]" : "";
        while (true) {
            System.out.printf("Harga (nilai harus positif)\t %s : ", info);
            String input = scanner.nextLine();
            try {

                if (isEdit && (input.isEmpty() || input.isBlank())) return (Integer) object;
                return Integer.parseInt(scanner.nextLine());
            } catch (Exception e) {
                continue;
            }
        }
    }
    public static void voidScanner(String info) {
        System.out.printf("%s\n", info);
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
    }
}
