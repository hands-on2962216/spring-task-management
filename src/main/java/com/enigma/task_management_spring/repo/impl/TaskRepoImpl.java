package com.enigma.task_management_spring.repo.impl;

import com.enigma.task_management_spring.entity.Task;
import com.enigma.task_management_spring.repo.TaskRepo;
import jakarta.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TaskRepoImpl implements TaskRepo {
    private final EntityManager em;

    @Autowired
    public TaskRepoImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public void save(Task task) {
        em.getTransaction().begin();
        em.persist(task);
        em.getTransaction().commit();
    }
}
