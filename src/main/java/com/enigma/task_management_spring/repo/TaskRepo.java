package com.enigma.task_management_spring.repo;

import com.enigma.task_management_spring.entity.Task;

public interface TaskRepo {
    void save(Task task);
}
